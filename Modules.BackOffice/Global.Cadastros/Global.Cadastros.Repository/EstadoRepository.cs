﻿using Dapper;
using MegaERP.Core.Interfaces;
using System.Collections.Generic;

namespace Mega.Erp.Global.Estado
{
    public class EstadoRepository : MegaERPRepository<Estado>, IEstadoRepository
    {
        public EstadoRepository(IMegaERPConnection _megaerpconnection) : base(_megaerpconnection) { }

        public IEnumerable<Estado> GetAll(EstadoRequestDTO dto)
        {
            string select = "select * from mgglo.glo_uf";
            return _connection.Query<Estado>(select);
        }

        public Estado GetEstado(EstadoRequestDTO dto)
        {
            string select = "select * from mgglo.glo_uf where uf_st_sigla = :uf";
            return _connection.QueryFirst<Estado>(select, new { uf = dto.uf });
        }
    }
}
