﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mega.Erp.Global.Estado
{
    public class RepositoryRegistry: Registry
    {
        public RepositoryRegistry()
        {
            For<IEstadoRepository>().Use<EstadoRepository>();
        }
    }
}
