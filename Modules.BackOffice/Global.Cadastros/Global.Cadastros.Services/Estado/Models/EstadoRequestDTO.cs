﻿using System;
using System.Collections.Generic;
using System.Text;
using MegaERP.Core.Interfaces;

namespace Mega.Erp.Global.Estado
{
    public class EstadoRequestDTO : MegaERPRequestDTO
    {
        public string uf {get; set;}
    }
}
