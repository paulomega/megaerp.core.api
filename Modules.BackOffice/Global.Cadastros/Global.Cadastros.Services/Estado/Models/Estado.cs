﻿using System;

namespace Mega.Erp.Global.Estado
{
    public class Estado
    {
        public string uf_st_sigla { get; set; } 
        public string uf_st_nome { get; set; } 
    }
}
