﻿using MegaERP.Core.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mega.Erp.Global.Estado
{
    public class EstadoService
    {
        public IEstadoRepository _repo { get; set; }

        public EstadoService(IEstadoRepository _repository)
        {
            _repo = _repository;
        }

        public MegaERPResponseDTO estadoresponse(dynamic funcao)
        {
            return new MegaERPResponseDTO { Data = funcao };
        }

        public MegaERPResponseDTO GetAll(EstadoRequestDTO requestDTO)
        {
            Func<dynamic, MegaERPResponseDTO> retorno = estadoresponse; 
            return retorno(_repo.GetAll(requestDTO));
        }

        public MegaERPResponseDTO GetEstado(EstadoRequestDTO requestDTO)
        {
            return new MegaERPResponseDTO { Data = _repo.GetEstado(requestDTO) };
        }


        public dynamic GetAllRaw(EstadoRequestDTO requestDTO)
        {
            return _repo.GetAll(requestDTO);
        }

        public dynamic GetEstadoRaw(EstadoRequestDTO requestDTO)
        {
            return _repo.GetEstado(requestDTO);
        }

    }
}
