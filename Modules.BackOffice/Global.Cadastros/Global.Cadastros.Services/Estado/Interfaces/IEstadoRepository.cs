﻿using MegaERP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mega.Erp.Global.Estado
{
    public interface IEstadoRepository : IMegaERPRepository<Estado>
    {
        IEnumerable<Estado> GetAll(EstadoRequestDTO dto);
        Estado GetEstado(EstadoRequestDTO dto);
    }
}
