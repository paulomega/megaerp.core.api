﻿using MegaERP.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Mega.Erp.Global.Estado
{
    [Route("api/v1/filial/{filial}/[controller]")]
    public class EstadoController: Controller
    {
        private EstadoService estadoService;
        private IMegaERPResponseDTO megaerpResponseDTO;

        public EstadoController(IMegaERPResponseDTO _megaerpResponseDTO, EstadoService _estadoService)
        {
            megaerpResponseDTO = _megaerpResponseDTO;
            estadoService = _estadoService;
        }
/*
        [HttpGet()]
        public MegaERPResponseDTO GetAll(EstadoRequestDTO dto)
        {
            return estadoService.GetAll(dto); 
        }

        [HttpGet("{uf:alpha}", Name = "GetEstado")]
        public MegaERPResponseDTO Get(EstadoRequestDTO dto)
        {
            return estadoService.GetEstado(dto);
        }
*/

        [HttpGet()]
        public dynamic GetAll(EstadoRequestDTO dto)
        {
            return megaerpResponseDTO.GetResponse(estadoService.GetAllRaw(dto));
        }

        [HttpGet("{uf:alpha}", Name = "GetEstado")]
        public dynamic Get(EstadoRequestDTO dto)
        {
            return megaerpResponseDTO.GetResponse(estadoService.GetEstadoRaw(dto));
        }

    }
}
