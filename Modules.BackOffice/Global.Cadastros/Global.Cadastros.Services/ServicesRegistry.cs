﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mega.Erp.Global.Estado
{
    public class ServicesRegistry : Registry
    {
        public ServicesRegistry()
        {
            For<EstadoService>().Use<EstadoService>();
        }
    }
}
