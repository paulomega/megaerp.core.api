﻿using Mega.Erp.Global.Estado;
using MegaERP.Core.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mega.Erp.Financ.TipoDocumento
{
    public class TipoDocumentoService
    {
        ITipoDocumentoRepository _repo { get; set; }
        EstadoService estadoService;

        public TipoDocumentoService(ITipoDocumentoRepository _repository, EstadoService _estadoService)
        {
            _repo = _repository;
            estadoService = _estadoService;
        }

        public MegaERPResponseDTO estadoresponse(dynamic funcao)
        {
            return new MegaERPResponseDTO { Data = funcao };
        }

        public MegaERPResponseDTO GetAll(TipoDocumentoRequestDTO requestDTO)
        {
            Func<dynamic, MegaERPResponseDTO> retorno = estadoresponse; 
            return retorno(_repo.GetAll(requestDTO));
        }

        public MegaERPResponseDTO GetEstado(TipoDocumentoRequestDTO requestDTO)
        {
            return new MegaERPResponseDTO { Data = _repo.GetEstado(requestDTO) };
        }


        public dynamic GetAllRaw(TipoDocumentoRequestDTO requestDTO)
        {
            //   return _repo.GetAll(requestDTO);
            return estadoService.GetAllRaw(new EstadoRequestDTO() );
        }

        public dynamic GetEstadoRaw(TipoDocumentoRequestDTO requestDTO)
        {
            return _repo.GetEstado(requestDTO);
        }

    }
}
