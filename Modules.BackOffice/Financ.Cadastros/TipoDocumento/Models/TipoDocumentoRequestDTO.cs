﻿using System;
using System.Collections.Generic;
using System.Text;
using MegaERP.Core.Interfaces;

namespace Mega.Erp.Financ.TipoDocumento
{
    public class TipoDocumentoRequestDTO : MegaERPRequestDTO
    {
        public string uf {get; set;}
    }
}
