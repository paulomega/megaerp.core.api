﻿using System;

namespace Mega.Erp.Financ.TipoDocumento
{
    public class TipoDocumento
    {
        public string tpd_st_codigo { get; set; } 
        public string tpd_st_descricao { get; set; } 
    }
}
