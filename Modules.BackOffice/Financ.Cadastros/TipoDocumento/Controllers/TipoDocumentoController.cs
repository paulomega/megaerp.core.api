﻿using MegaERP.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Mega.Erp.Financ.TipoDocumento
{
    [Route("api/v1/filial/{filial}/[controller]")]
    public class TipoDocumentoController: Controller
    {
        private TipoDocumentoService tipodocumentoService;
        private IMegaERPResponseDTO megaerpResponseDTO;

        public TipoDocumentoController(IMegaERPResponseDTO _megaerpResponseDTO, TipoDocumentoService _estadoService)
        {
            megaerpResponseDTO = _megaerpResponseDTO;
            tipodocumentoService = _estadoService;
        }
/*
        [HttpGet()]
        public MegaERPResponseDTO GetAll(EstadoRequestDTO dto)
        {
            return estadoService.GetAll(dto); 
        }

        [HttpGet("{uf:alpha}", Name = "GetEstado")]
        public MegaERPResponseDTO Get(EstadoRequestDTO dto)
        {
            return estadoService.GetEstado(dto);
        }
*/

        [HttpGet()]
        public dynamic GetAll(TipoDocumentoRequestDTO dto)
        {
            return megaerpResponseDTO.GetResponse(tipodocumentoService.GetAllRaw(dto));
        }

        [HttpGet("{uf:alpha}", Name = "GetTipoDocumento")]
        public dynamic Get(TipoDocumentoRequestDTO dto)
        {
            return megaerpResponseDTO.GetResponse(tipodocumentoService.GetEstadoRaw(dto));
        }

    }
}
