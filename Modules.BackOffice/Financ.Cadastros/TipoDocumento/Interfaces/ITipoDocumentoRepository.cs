﻿using MegaERP.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mega.Erp.Financ.TipoDocumento
{
    public interface ITipoDocumentoRepository : IMegaERPRepository<TipoDocumento>
    {
        IEnumerable<TipoDocumento> GetAll(TipoDocumentoRequestDTO dto);
        TipoDocumento GetEstado(TipoDocumentoRequestDTO dto);
    }
}
