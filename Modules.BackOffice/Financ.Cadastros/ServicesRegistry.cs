﻿using Mega.Erp.Financ.TipoDocumento;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mega.Erp.Financ
{
    public class ServicesRegistry : Registry
    {
        public ServicesRegistry()
        {
            For<TipoDocumentoService>().Use<TipoDocumentoService>();
        }
    }
}
