﻿using Dapper;
using MegaERP.Core.Interfaces;
using System.Collections.Generic;

namespace Mega.Erp.Financ.TipoDocumento
{
    public class TipoDocumentoRepository : MegaERPRepository<TipoDocumento>, ITipoDocumentoRepository
    {
        public TipoDocumentoRepository(IMegaERPConnection _megaerpconnection) : base(_megaerpconnection) { }

        public IEnumerable<TipoDocumento> GetAll(TipoDocumentoRequestDTO dto)
        {
            string select = "select * from mgfin.fin_tipo_documento";
            return _connection.Query<TipoDocumento>(select);
        }

        public TipoDocumento GetEstado(TipoDocumentoRequestDTO dto)
        {
            string select = "select * from mgfin.fin_tipo_documento where tpd_st_codigo = :uf";
            return _connection.QueryFirst<TipoDocumento>(select, new { uf = dto.uf });
        }
    }
}
