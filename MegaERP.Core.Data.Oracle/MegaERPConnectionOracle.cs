﻿using MegaERP.Core.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;

namespace MegaERP.Core.Data.Oracle
{
    public class MegaERPConnectionOracle : IMegaERPConnection, IDisposable
    {
        private DbConnection connection { get; set; }

        public MegaERPConnectionOracle(IConfiguration config)
        {
            var connectString = config["Oracle:ConnectString"];
            connection = new OracleConnection(connectString);
            connection.Open();
        }

        public void Dispose()
        {
            connection.Close();
        }

        public DbConnection GetConnection()
        {
            return connection;
        }
    }
}
