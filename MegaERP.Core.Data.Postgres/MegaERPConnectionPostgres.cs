﻿using MegaERP.Core.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Data.Common;

namespace MegaERP.Core.Data.Postgres
{
    public class MegaERPConnectionPostgres : IMegaERPConnection, IDisposable
    {
        private DbConnection connection { get; set; }

        public MegaERPConnectionPostgres(IConfiguration config)
        {
            var connectString = config["Postgres:ConnectString"];
            connection = new Npgsql.NpgsqlConnection(connectString);
            connection.Open();
        }

        public void Dispose()
        {
            connection.Close();
        }

        public DbConnection GetConnection()
        {
            return connection;
        }
    }
}
