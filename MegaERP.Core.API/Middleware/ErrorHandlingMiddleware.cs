﻿using MegaERP.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MegaERP.Core.API.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other scoped dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            int code = context.Response.StatusCode;
            if (code == 200)
                code = 500;
            MegaERPResponseExceptionDTO responseDTO = new MegaERPResponseExceptionDTO();
            responseDTO.HttpStatusCode = code;
            responseDTO.ExceptionType = exception.GetType().ToString();
            responseDTO.Message = exception.Message;
            responseDTO.StackTrace = exception.StackTrace.Substring(0, exception.StackTrace.IndexOf("---"));
            var result = JsonConvert.SerializeObject(responseDTO);
            context.Response.ContentType = "application/json";
            return context.Response.WriteAsync(result);
        }
    }
}
