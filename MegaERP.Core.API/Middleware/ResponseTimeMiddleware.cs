﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MegaERP.Core.API.Middleware
{
    public static class ResponseTimeExtensions
    {
        public static IApplicationBuilder UseApiResponse(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ResponseTimeMiddleware>();
        }
    }

    public class RequestInfo
    {
        public Stopwatch _stopWatch;
        public HttpContext _context;
    }

    public class ResponseTimeMiddleware
    {
        private readonly RequestDelegate _next;

        public ResponseTimeMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var requestInfo = new RequestInfo();
            requestInfo._stopWatch = Stopwatch.StartNew();
            requestInfo._context = context;
            context.Response.OnStarting(OnStartingCallback, state: requestInfo);
            await _next.Invoke(context);
        }

        public Task OnStartingCallback(object state)
        {
            var requestInfo = ((RequestInfo)state);
            requestInfo._stopWatch.Stop();
            requestInfo._context.Response.Headers.Add("X-Response-Time", new string[] { requestInfo._stopWatch.ElapsedMilliseconds.ToString() + "ms" });
            return Task.FromResult(0);
        }

    }
}
