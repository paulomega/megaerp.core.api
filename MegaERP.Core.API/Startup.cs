﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StructureMap;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MegaERP.Core.API.Controllers;
using System.Reflection;
using System.Runtime.Loader;
using System.IO;
using Microsoft.Extensions.DependencyModel;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using System.Text;
using MegaERP.Core.Interfaces;
#if NET462
using MegaERP.Core.Data.Oracle;
#else
using MegaERP.Core.Data.Postgres;
#endif
using MegaERP.Core.API.Middleware;

namespace MegaERP.Core.API
{
    public class Startup
    {
        IHostingEnvironment _hostingEnvironment;
        private readonly IList<ModuleInfo> modules = new List<ModuleInfo>();
        string modulesRootPath;

        public Startup(IHostingEnvironment env)
        {
            _hostingEnvironment = env;

            var builder = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                    .AddJsonFile("hosting.json", optional: false, reloadOnChange: true)
                    .AddEnvironmentVariables();
                Configuration = builder.Build();

             this.modulesRootPath = Path.Combine(_hostingEnvironment.ContentRootPath, "modules");
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IConfiguration>(Configuration);

            var moduleRootFolder = new DirectoryInfo(this.modulesRootPath);
            var moduleFolders = moduleRootFolder.GetDirectories();

            Console.WriteLine(this.modulesRootPath);

            foreach (var moduleFolder in moduleFolders)
            {
                foreach (var file in moduleFolder.GetFileSystemInfos("*.dll", SearchOption.AllDirectories))
                {
                    Console.WriteLine(file.FullName);

                    Assembly assembly = null;
                    try
                    {
//                        assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(file.FullName);
                        assembly = Assembly.LoadFrom(file.FullName);
                    }
                    catch (FileLoadException)
                    {
                        throw;
                    }
                    modules.Add(new ModuleInfo { Name = moduleFolder.Name, Assembly = assembly, Path = moduleFolder.FullName });
                }
            }

            var mvcBuilder = services.AddMvc();
            foreach (var module in modules)
            {
                if (module.Assembly.FullName.ToLower().Contains("services"))
                    // Register controller from modules
                    mvcBuilder.AddApplicationPart(module.Assembly);
            }

        }

        public class MegaERPCoreRegistry : Registry
        {
            public MegaERPCoreRegistry()
            {
                For<IMegaERPResponseDTO>().Use<MegaERPResponseDTO>();
#if NET462
                For<IMegaERPConnection>().Use<MegaERPConnectionOracle>();
#else
                For<IMegaERPConnection>().Use<MegaERPConnectionPostgres>();
#endif

            }
        }

        public void ConfigureContainer(Registry registry)
        {
            registry.IncludeRegistry<MegaERPCoreRegistry>();

            // Use StructureMap-specific APIs to register services in the registry.
            registry.Scan(x => {
                // x.AssembliesAndExecutablesFromPath(this.modulesRootPath, assembly => assembly.FullName.ToLower().Contains("services"));
                // x.AssembliesAndExecutablesFromPath(this.modulesRootPath);
                foreach (var module in modules)
                {
                    x.Assembly(module.Assembly);    
                }
                x.LookForRegistries();
                x.WithDefaultConventions();
            }
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseApiResponse();
            app.UseMvc();
        }
    }

    public class ModuleInfo
    {
        public string Name { get; set; }

        public Assembly Assembly { get; set; }

        public string SortName
        {
            get
            {
                return Name.Split('.').Last();
            }
        }

        public string Path { get; set; }
    }

}
