﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace MegaERP.Core.Interfaces
{
    public class MegaERPRepository<T> : IMegaERPRepository<T>, IDisposable
    {
        public IDbConnection _connection { get; }
        public string _tablename { get; set; }

        public MegaERPRepository(IMegaERPConnection _megaerpconnection)
        {
            _connection = _megaerpconnection.GetConnection();
        }

        public T Get(T entity)
        {
            return _connection.QueryFirst<T>(string.Concat("Select * From ", _tablename));
        }

        public int Insert(T entity)
        {
            return 1;
        }

        public void Delete(T entity)
        {
        }

        public int Update(T entity)
        {
            return 1;
        }

        public List<T> Busca(string value)
        {
            return new List<T>();
        }

        public void Dispose()
        {
        }
    }
}
