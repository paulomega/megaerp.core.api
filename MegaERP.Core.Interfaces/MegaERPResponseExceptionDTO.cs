﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MegaERP.Core.Interfaces
{
    public class MegaERPResponseExceptionDTO
    {
        public bool Status { get; set; }
        public int HttpStatusCode { get; set; }
        public string ExceptionType { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public MegaERPResponseExceptionDTO() 
        {
            Status = false;
            HttpStatusCode = 500;
        }
    }
}
