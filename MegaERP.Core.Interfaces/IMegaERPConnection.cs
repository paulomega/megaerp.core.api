﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace MegaERP.Core.Interfaces
{
    public interface IMegaERPConnection
    {
        DbConnection GetConnection();
    }
}
