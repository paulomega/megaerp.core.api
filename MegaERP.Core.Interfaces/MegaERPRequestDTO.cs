﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MegaERP.Core.Interfaces
{
    public class MegaERPRequestDTO
    {
        public DateTime Data { get; set; }
        public int Filial { get; set; }
        public int PageNumber { get; set; }
        public int Lines { get; set; }

        public MegaERPRequestDTO()
        {
            Data = DateTime.Now;
        }
    }
}
