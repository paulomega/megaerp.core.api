﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace MegaERP.Core.Interfaces
{
    public interface IMegaERPRepository<T> : IDisposable
    {
        IDbConnection _connection { get; }
        String _tablename { get; set; }
        T Get(T entity);
        int Insert(T entity);
        void Delete(T entity);
        int Update(T entity);
        List<T> Busca(string value);
    }
}
