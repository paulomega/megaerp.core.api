﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MegaERP.Core.Interfaces
{
    public interface IMegaERPResponseDTO
    {
        IMegaERPResponseDTO GetResponse(dynamic getService);
    }
}
