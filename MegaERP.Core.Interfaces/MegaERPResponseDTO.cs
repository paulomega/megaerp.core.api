﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Text;

namespace MegaERP.Core.Interfaces
{
    public class MegaERPResponseDTO : IMegaERPResponseDTO, IDisposable
    {
        public bool Status { get; set; }
        public int HttpStatusCode { get; set; }
        public dynamic Data { get; set; }
        public TimeSpan ExecTime { get; set; }
        public long ExecTimeMilliseconds { get; set; }

        private Stopwatch _stopWatch;

        public MegaERPResponseDTO()
        {
            _stopWatch = Stopwatch.StartNew();
            Status = true;
            HttpStatusCode = 200;
        }

        public IMegaERPResponseDTO GetResponse(dynamic getService)
        {
            try
            {
                this.Data = getService;
            }
            finally
            {
                _stopWatch.Stop();
                this.ExecTime = _stopWatch.Elapsed;
                this.ExecTimeMilliseconds = _stopWatch.ElapsedMilliseconds;
            }

            return this;
        }

        public void Dispose()
        {
        }
    }
}
